# The way of the automation

## Some improvements over the script

```bash
#!/bin/bash

set -euo pipefail

# Variable definitions
packages=(sudo git htop curl wget vim tmux)
user_name="jon"
user_full_name="Jon Snow"
user_group="jon"
user_groups="${user_group},users"
user_uid=1001
user_gid=1001
sudoers_file="/etc/sudoers.d/$user_name"

# Install dependencies
sudo dnf install -y "${packages[@]}"

# Create group
sudo groupadd -g "$user_gid" "$user_group"

# Create user
sudo adduser -c "$user_full_name" -g "$user_gid" -u "$user_uid" \
             -G "$user_groups" "$user_name"

# Add user to sudoers
echo "${user_name} ALL = (ALL:ALL) NOPASSWD: ALL" \
  | sudo tee -a "$sudoers_file"
sudo chmod 0440 "$sudoers_file"

# Check if the new sudoers file is correct
sudo visudo -cf "$sudoers_file"
```
