# Introduction to Ansible

This repo contains some examples and references to understand
how [Ansible](https://docs.ansible.com/) works, what kind of 
problems solves, and how to write your first Ansible playbook
using a simple script as starting point.

## From scripts to Ansible

To understand better what Ansible is and what it does, it's
better to start with simples and common problems that we usually
solve and automate with scripts.

At the file [The way of the automation](cli-to-script.md) there are
some examples and their typical evolution.

After that you can see the equivalent in Ansible at the directory
[playbooks](playbooks/).

It tries to go from the simplest version, the more similar to the scripts,
to the more complex, but still simple, version.

### Run the examples

It is advisable to try the examples in a virtual machine or
disposable server. Somewhere where we has access to the root account
and we can break things.

To run the scripts just copy them at that testing machine and
run them.

For the Ansible plays you will need to install Ansible at your machine
and configure add the testing machine to your inventory:

### Install Ansible

You can use a package from tyour system:

```shell
$ sudo dnf install -y ansible
```

or you can use `pip` to install it as Python package:
```shell
$ pip install --user ansible
```

### The testing machine requirements

In order to use Ansible in one machine, you need to be able to connect
via SSH to that machine and to have Python installed.

And to install software or do some system configurations, you need access
to the admin account (e.g. `root`) or an user with administrative privileges
(e.g. with `sudo` access).

### Add machine to the inventory

Let's say that you have a testing VM or remote machine with IP `192.168.121.82`
and it has a user called `vagrant` with `sudo` access. And you want to call
that host `testing_ansible`.

Also, let's say that you access to that machine using a SSH private key
located at `~/.ssh/vagrant_private_key`.

For this case, you could create a inventary file like this:

```ini
[all]
testing_ansible ansible_host=192.168.121.82

[all:vars]
ansible_user=vagrant
ansible_ssh_private_key_file=~/.ssh/vagrant_private_key

```

We'll name this file `hosts`, but it can be any name.

### Run the playbooks

Now you can run the playbooks like so (from your machine):
```shell
$ ansible-playbook -i hosts plaubooks/01-basic.yml
```

> **NOTE:** The option `-i` is the same as `--inventory` which indicate
the file where the hosts that we want to configure are listed. The file fromt the secion
above. 

## Examples

Also there are some more real life examples. There you can see more
features of Ansible and how to do different kind of tasks.

* [Gitlab-runner](examples/gitlab-runner): Install and configure a Gitlab-runner.
It will use different method depending on the architecture.
* [Container register](examples/container-registry.yml): Good example to see how
to use commands inside Ansible, when you don't have module for a specific task.
* [Gnome config](examples/gnome.yml): Example of how we can use Ansible to
configure our desktop and how to use dictionaries in loops.

## References

### Official documentation

* [Official doc site](https://docs.ansible.com/ansible/latest/)
* [Starting guide](https://docs.ansible.com/ansible/latest/user_guide/index.html#getting-started)

#### Some useful modules and features

* [Inventory](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)
* [Debug module](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/debug_module.html)
* [Loops](https://docs.ansible.com/ansible/latest/user_guide/playbooks_loops.html)
* [Conditionals](https://docs.ansible.com/ansible/latest/user_guide/playbooks_conditionals.html)
* [Filters](https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters.html)
* Modules [command](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/command_module.html)
and [shell](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/shell_module.html).


### Ansible 101 by Jeff Geerling

* [YouTube playlist](https://www.youtube.com/playlist?list=PL2_OBreMn7FqZkvMYt6ATmgC0KAGGJNAN)
* Book: [Ansible for DevOps](https://www.ansiblefordevops.com/)

### Ansible for networking

* [Ansible Networking starting guide](https://docs.ansible.com/ansible/latest/network/getting_started/)
* Video: [get started with Ansible Network Automation (FREE cisco router lab)](https://www.youtube.com/watch?v=OWKPxAgh9DU)


