# The way of the automation

## At the begining it was the command line

### Install dependencies

```shell
sudo dnf install -y git htop curl wget vim tmux
```

### Create the user's group

```shell
sudo groupadd -g 1001 jon
```

### Create the user's account

```shell
sudo adduser -c "Jon Snow" -g 1001 -u 1001 -G jon,users jon
```

### Add the user to the sudo (without password)

```shell
echo "jon ALL = (ALL:ALL) NOPASSWD: ALL" | sudo tee -a /etc/sudoers.d/jon
sudo chmod 0440 /etc/sudoers.d/jon
```

## Then the script

```bash
#!/bin/bash

# Install dependencies
sudo dnf install -y git htop curl wget vim tmux

# Create group
sudo groupadd -g 1001 jon

# Create user
sudo adduser -c "Jon Snow" -g 1001 -u 1001 -G jon,users jon

# Add user to sudoers
echo "jon ALL = (ALL:ALL) NOPASSWD: ALL" | sudo tee -a /etc/sudoers.d/jon
sudo chmod 0440 /etc/sudoers.d/jon
```

## Some improvements over the script

```bash
#!/bin/bash

set -euo pipefail

# Variable definitions
packages=(sudo git htop curl wget vim tmux)
user_name="jon"
user_full_name="Jon Snow"
user_group="jon"
user_groups="${user_group},users"
user_uid=1001
user_gid=1001
sudoers_file="/etc/sudoers.d/$user_name"

# Install dependencies
sudo dnf install -y "${packages[@]}"

# Create group
sudo groupadd -g "$user_gid" "$user_group"

# Create user
sudo adduser -c "$user_full_name" -g "$user_gid" -u "$user_uid" \
             -G "$user_groups" "$user_name"

# Add user to sudoers
echo "${user_name} ALL = (ALL:ALL) NOPASSWD: ALL" \
  | sudo tee -a "$sudoers_file"
sudo chmod 0440 "$sudoers_file"

# Check if the new sudoers file is correct
sudo visudo -cf "$sudoers_file"
```

## Make it more robust (idemponent?)

```bash
#!/bin/bash

set -euo pipefail

# Variable definitions
packages=(sudo git htop curl wget vim tmux)
user_name="jon"
user_full_name="Jon Snow"
user_group="jon"
user_groups="${user_group},users"
user_uid=1001
user_gid=1001
sudoers_file="/etc/sudoers.d/$user_name"

if [ "$(id -u)" != 0 ]; then
  echo "You need to be root or use 'sudo' for using this sctip"
  exit 1
fi


# Install dependencies
echo "* Installing packages..."
dnf install -y "${packages[@]}"

# Create group
echo -ne "* Adding the group ${user_group}...\t"
if grep -q "$user_name" /etc/group ; then
  echo "The group ${user_group} already exist. Noting to do." 
else
  groupadd -g "$user_gid" "$user_name"
  echo "Ok"
fi

# Create user
echo -ne "* Adding the user account...\t"
if grep -q "$user_name" /etc/passwd ; then
  echo "The user ${user_name} already exist. Noting to do." 
else
  adduser -c "$user_full_name" -g "$user_gid" -u "$user_uid" \
          -G "$user_groups" "$user_name"
  echo "Ok"
fi

# Add user to sudoers
echo -ne "* Adding the user ${user_name} to the sudoers...\t"
if [ -f "$sudoers_file" ]; then
  echo "The file ${sudoers_file} already exist. Nothing to do."
else
  echo "${user_name} ALL = (ALL:ALL) NOPASSWD: ALL" > "$sudoers_file"
  chmod 0440 "$sudoers_file"
  echo "Ok"
fi

# Check if the new sudoers file is correct
echo -ne "* Checking the sudores file is correct...\t"
visudo -cf "$sudoers_file"

```

## Add more users

```bash
#!/bin/bash

set -euo pipefail

# Variable definitions
packages=(sudo git htop curl wget vim tmux)
declare -A user_1
user_1=(
  [name]="jon"
  [full_name]="Jon Snow"
  [group]="jon"
  [groups]="${user_1[group]},users"
  [uid]=1001
  [gid]=1001
)
declare -A user_2
user_2=(
  [name]="arya"
  [full_name]="Arya Stark"
  [group]="arya"
  [groups]="${user_2[group]},users"
  [uid]=1002
  [gid]=1002
)

if [ "$(id -u)" != 0 ]; then
  echo "You need to be root or use 'sudo' for using this sctip"
  exit 1
fi


# Install dependencies
echo "* Installing packages..."
dnf install -y "${packages[@]}"

for n in 1 2; do
  # Some bash black magick
  declare -n user="user_$n"

  # Create group
  echo -ne "* Adding the group ${user[group]}...\t"
  if grep -q "${user[name]}" /etc/group ; then
    echo "The group ${user[group]} already exist. Noting to do." 
  else
    groupadd -g "${user[gid]}" "${user[name]}"
    echo "Ok"
  fi

  # Create user
  echo -ne "* Adding the user account...\t"
  if grep -q "${user[name]}" /etc/passwd ; then
    echo "The user ${user[name]} already exist. Noting to do." 
  else
    adduser -c "${user[full_name]}" -g "${user[gid]}" -u "${user[uid]}" \
            -G "${user[groups]}" "${user[name]}"
    echo "Ok"
  fi

  # Add user to sudoers
  sudoers_file="/etc/sudoers.d/${user[name]}"
  echo -ne "* Adding the user ${user[name]} to the sudoers...\t"
  if [ -f "$sudoers_file" ]; then
    echo "The file ${sudoers_file} already exist. Nothing to do."
  else
    echo "${user[name]} ALL = (ALL:ALL) NOPASSWD: ALL" > "$sudoers_file"
    chmod 0440 "$sudoers_file"
    echo "Ok"
  fi

  # Check if the new sudoers file is correct
  echo -ne "* Checking the sudores file is correct...\t"
  visudo -cf "$sudoers_file"
done
```

## From the script to Ansible

All this can be done easier with Ansible because it has some
features that can help us:

* It has plenty of modules to do all the common tasks and more.
* Those modules are idempotent by default.
* Also, they've been tested by a large community.
* Ansible make available to be used a lot of information about
the system (arch, OS version, netwotk information...) as variables.
* It can run in any system that can run Python (servers, routers,
cloud, VMs, RasperryPi...).
* It can run in parallel on diferent machines and adapt easily
to the specifics of each one.

Here are the equivalent to the previous script, but in Ansible:
[playbooks](playbooks)


